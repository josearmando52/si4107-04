A.Pendahuluan
Latar Belakang

Dalam memenuhi tugas mata kuliah RPL. Kami akan membuat suatu aplikasi yang bernama
BangunDesa dimana proyek ini ditujukan sebagai tempat mengumpulkan orang-orang yang memiliki 
ide untuk membangun suatu desa dengan orang-orang yang ingin menyumbangkan uangnya untuk mebantu berjalannya proyek pembangunan desa.

Deskripsi Produk

Aplikasi BangunDesa terdiri dari beberapa fitur, yaitu fitur Login, Fitur membuat projek, fitur donasi dan fitur menampilkan projek. 
Tujuan pengembangan Aplikasi BangunDesa adalah untuk mengumpulkan orang-orang yang memiliki ide untuk membangun suatu desa dengan 
orang-orang yang ingin menyumbangkan uangnya untuk mebantu berjalannya proyek pembangunan desa